# cf-event-audit

[![Actions Status][actions-badge]][actions-link]
[![Documentation Status][rtd-badge]][rtd-link]
[![Code style: black][black-badge]][black-link]

[![PyPI version][pypi-version]][pypi-link]
[![Conda-Forge][conda-badge]][conda-link]
[![PyPI platforms][pypi-platforms]][pypi-link]

[![GitHub Discussion][github-discussions-badge]][github-discussions-link]
[![Gitter][gitter-badge]][gitter-link]




[actions-badge]:            https://github.com/conda-forge/cf-event-audit/workflows/CI/badge.svg
[actions-link]:             https://github.com/conda-forge/cf-event-audit/actions
[black-badge]:              https://img.shields.io/badge/code%20style-black-000000.svg
[black-link]:               https://github.com/psf/black
[conda-badge]:              https://img.shields.io/conda/vn/conda-forge/cf-event-audit
[conda-link]:               https://github.com/conda-forge/cf-event-audit-feedstock
[github-discussions-badge]: https://img.shields.io/static/v1?label=Discussions&message=Ask&color=blue&logo=github
[github-discussions-link]:  https://github.com/conda-forge/cf-event-audit/discussions
[gitter-badge]:             https://badges.gitter.im/https://github.com/conda-forge/cf-event-audit/community.svg
[gitter-link]:              https://gitter.im/https://github.com/conda-forge/cf-event-audit/community?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge
[pypi-link]:                https://pypi.org/project/cf-event-audit/
[pypi-platforms]:           https://img.shields.io/pypi/pyversions/cf-event-audit
[pypi-version]:             https://badge.fury.io/py/cf-event-audit.svg
[rtd-badge]:                https://readthedocs.org/projects/cf-event-audit/badge/?version=latest
[rtd-link]:                 https://cf-event-audit.readthedocs.io/en/latest/?badge=latest
[sk-badge]:                 https://scikit-hep.org/assets/images/Scikit--HEP-Project-blue.svg
