__all__ = ("__version__", "app")

import hashlib
import hmac
import logging
import os
import time
from copy import deepcopy
from dataclasses import dataclass
from datetime import datetime
from ssl import create_default_context
from typing import Any

from fastapi import Depends, FastAPI, Header, HTTPException, Request, status
from opensearchpy import AsyncOpenSearch

from .version import version as __version__

app = FastAPI()

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
formatter.converter = time.gmtime  # type: ignore
console_handler = logging.StreamHandler()
console_handler.setLevel(logger.level)
console_handler.setFormatter(formatter)
logger.addHandler(console_handler)

opensearch = AsyncOpenSearch(
    os.environ["OPENSEARCH_HOST"],
    http_auth=(os.environ["OPENSEARCH_USER"], os.environ["OPENSEARCH_PASS"]),
    http_compress=True,
    ssl_context=create_default_context(
        capath="/cvmfs/lhcb.cern.ch/etc/grid-security/certificates"
    ),
)
OPENSEARCH_INDEX_PREFIX = os.environ["OPENSEARCH_INDEX_PREFIX"]


@app.get("/")
async def root() -> dict[str, str]:
    return {"message": "Hello World"}


@dataclass
class GitHubWebhookPayload:
    guid: str
    event_name: str
    body: str
    data: dict[str, Any]


async def validate_github_webhook(
    request: Request,
    x_github_event: str = Header(None),
    x_github_delivery: str = Header(None),
    x_hub_signature_256: str = Header(None),
) -> GitHubWebhookPayload:
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    logger.info(
        "Received payload %s of type %s with signature %s",
        x_github_event,
        x_github_delivery,
        x_hub_signature_256,
    )
    if x_github_event and x_hub_signature_256 and x_github_delivery:
        _, actual_signature = x_hub_signature_256.split("=")
        expected_signature = hmac.new(
            os.environ["CF_WEBSERVICES_TOKEN"].encode("utf-8"),
            await request.body(),
            hashlib.sha256,
        ).hexdigest()
        if hmac.compare_digest(actual_signature, expected_signature):
            return GitHubWebhookPayload(
                guid=x_github_delivery,
                event_name=x_github_event,
                body=(await request.body()).decode(),
                data=await request.json(),
            )

    raise credentials_exception


def cleanup_payload(obj: dict[str, Any], prefix: str = "") -> None:
    """Clean a GitHub payload to remove fields which are pointless to index"""
    for k in list(obj):
        v = obj[k]
        if isinstance(v, dict):
            # Recurse deeper if it's a dictionary
            cleanup_payload(v, prefix=f"{prefix}.{k}")
        elif isinstance(v, list):
            # Remove lists
            del obj[k]
        elif isinstance(v, str) and (k in {"url", "href"} or k.endswith("_url")):
            # Remove URLs
            del obj[k]


@app.post("/payloads/github")
async def github(
    payload: GitHubWebhookPayload = Depends(validate_github_webhook),
) -> Any:
    received_at = datetime.utcnow()

    doc = deepcopy(payload.data)
    cleanup_payload(doc)
    doc["received_at"] = received_at
    # Keep the original payload so we can losslessly reindex
    doc["payload"] = payload.body
    doc["payload_size"] = len(payload.body)
    index_name = f"{OPENSEARCH_INDEX_PREFIX}-{payload.event_name}-"
    index_name += datetime.utcnow().strftime("%Y-%m")
    res = await opensearch.index(index=index_name, body=doc, id=payload.guid)

    logger.info("Finishes processing request, inserted in opensearch as: %s", res)

    return res
