FROM centos:8

RUN yum install -y openssh-clients && \
    curl --silent -OL "https://github.com/conda-forge/miniforge/releases/latest/download/Miniforge3-Linux-x86_64.sh" && \
    bash "Miniforge3-Linux-x86_64.sh" -b -p "/miniconda" && \
    rm "Miniforge3-Linux-x86_64.sh" && \
    source "/miniconda/bin/activate" && \
    conda install --quiet -y "mamba" "git"

COPY environment.yaml /source/environment.yaml

RUN source "/miniconda/bin/activate" && \
    mamba env create --file "/source/environment.yaml" --name "cf-event-audit" && \
    conda activate "cf-event-audit"

COPY . /source/

RUN source "/miniconda/bin/activate" && \
    conda activate "cf-event-audit" && \
    pip install /source

ENV PATH=/miniconda/envs/cf-event-audit/bin:$PATH

USER 1000
WORKDIR /tmp
CMD ["uvicorn", "cf_event_audit:app", "--host", "0.0.0.0", "--port", "8000", "--use-colors", "--log-level", "info"]
EXPOSE 8000
